﻿using UnityEngine;
using System.Collections;

public class PickupItem : MonoBehaviour {

	public enum ItemType {
		CROWBAR,
		SHOTGUN,
		AMMO,
		HEALTH
	}

	public ItemType type;

	public void OnTriggerEnter(Collider other) {
		PlayerControl player = other.GetComponent<PlayerControl>();

		if (player != null) {
			// tell the player they have picked up this item.
			if (player.Pickup (this)) {
				// if successful, hide the object
				gameObject.SetActiveRecursively(false);
			}
		}
	}

}
