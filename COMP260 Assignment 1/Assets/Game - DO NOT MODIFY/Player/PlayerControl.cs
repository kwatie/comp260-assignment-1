﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	private Camera camera;
	public LayerMask zombieLayer;

	public enum Weapon { HANDS, CROWBAR, SHOTGUN }
	public bool hasCrowbar = false;
	public bool hasShotgun = false;
	public Weapon currentWeapon = Weapon.HANDS;

	private int ammo = 0;
	private float health = 1.0f;
	private bool dead = false;

	public int ammoSize = 5;
	public int shotgunAmmoSize = 2;
	public float healthSize = 0.2f;

	public Text healthText;
	public Text ammoText;

	public GameObject shotgun;
	public float shotgunDamage = 1.5f;	// enough to down, but not kill
	public float shotgunForce = 10.0f;
	public float shotgunRange = 20.0f;
	public float shotgunReload = 1.0f;  // DPS = 1.5
	public float shotgunNoise = 30.0f;  // DPS = 1.5

	public GameObject crowbar;
	public float crowbarDamage = 0.5f;
	public float crowbarForce = 2.0f;
	public float crowbarRange = 2.5f;
	public float crowbarReload = 0.2f; // DPS = 2.5

	public float handsDamage = 0.1f;
	public float handsForce = 2.0f;
	public float handsRange = 1.5f;
	public float handsReload = 0.2f; // DPS = 0.5

	private float reloadTimeout = 0f; 

	private float noise = 0;
	public float Noise {
		get {
			return noise;
		}
	}

	void Start() {
		camera = Camera.main;
	}

	void Update() {
		noise = 0;

		if (!dead) {
			if (reloadTimeout > 0) {
				reloadTimeout -= Time.deltaTime;
			}

			if (Input.GetButtonDown ("Fire1")) {
				Fire ();
			}

			if (Input.GetButtonDown ("Crowbar")) {
				Wield (Weapon.CROWBAR);
			}

			if (Input.GetButtonDown ("Shotgun")) {
				Wield (Weapon.SHOTGUN);
			}
		}
				
	}

	void Wield(Weapon weapon) {

		switch (weapon) {

		case Weapon.HANDS:
			currentWeapon = Weapon.HANDS;
			shotgun.SetActiveRecursively(false);
			crowbar.SetActiveRecursively(false);
			break;

		case Weapon.CROWBAR:
			shotgun.SetActiveRecursively (false);
			if (hasCrowbar) {
				currentWeapon = Weapon.CROWBAR;
				crowbar.SetActiveRecursively (true);
			} else {
				Wield(Weapon.HANDS);
			}
			break;

		case Weapon.SHOTGUN:
			// only works if you have the shotgun
			if (hasShotgun) {
				currentWeapon = Weapon.SHOTGUN;
				crowbar.SetActiveRecursively (false);
				shotgun.SetActiveRecursively (true);
			}
			break;
		}
	}

	void Fire() {

		// wait until reload timeout is over.
		if (reloadTimeout > 0) {
			return;
		}

		// weapon effects depend on which weapon is wielded

		float range = 0;
		float force = 0;
		float damage = 0;

		switch (currentWeapon) {
		case Weapon.HANDS:
			range = handsRange;
			force = handsForce;
			damage = handsDamage;
			reloadTimeout = handsReload;
			break;

		case Weapon.CROWBAR:
			range = crowbarRange;
			force = crowbarForce;
			damage = crowbarDamage;
			reloadTimeout = crowbarReload;
			break;

		case Weapon.SHOTGUN:
			if (ammo == 0) {
				return;
			}
			ammo--;
			ammoText.text = ammo.ToString();

			range = shotgunRange;
			force = shotgunForce;
			damage = shotgunDamage;
			reloadTimeout = shotgunReload;
			noise = shotgunNoise;
			break;
		}

		// cast a ray down the centre of the camera and see if it hits a zombie

		Vector3 aim = new Vector2(0.5f, 0.5f);
		Ray ray = camera.ViewportPointToRay(aim);

		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, range, zombieLayer)) {
			ZombieMove zombie = hit.rigidbody.GetComponent<ZombieMove>();
			zombie.Hit(damage, hit.point, ray.direction * force);
		}

	}

	public bool Pickup(PickupItem item) {

		if (dead) {
			return false;
		}

		switch (item.type) {
		case PickupItem.ItemType.AMMO:
			ammo += ammoSize;
			ammoText.text = ammo.ToString();
			break;

		case PickupItem.ItemType.CROWBAR:
			hasCrowbar = true;
			Wield(Weapon.CROWBAR);
			break;

		case PickupItem.ItemType.SHOTGUN:
			hasShotgun = true;
			Wield(Weapon.SHOTGUN);
			ammo += shotgunAmmoSize;
			ammoText.text = ammo.ToString();
			break;

		case PickupItem.ItemType.HEALTH:
			// don't pickup health if you don't need it
			if (health == 1.0f) {
				return false;
			}
			else {
				health = Mathf.Min(health + healthSize, 1.0f);
				healthText.text = Mathf.RoundToInt(health * 100) + "%";
			}
			break;
		}

		return true;
	}

	public void Hurt(float damage) {
		if (dead) {
			return;
		}
		health = Mathf.Max(0, health - damage);
		healthText.text = Mathf.RoundToInt(health * 100) + "%";

		if (health == 0) {
			Die ();
		}
	}

	void Die() {
		dead = true;
		RigidbodyFirstPersonController rfpc = GetComponent<RigidbodyFirstPersonController> ();
		rfpc.enabled = false;
		Rigidbody rigidbody = GetComponent<Rigidbody> (); 
		rigidbody.constraints = RigidbodyConstraints.None;
	}


}
