﻿using UnityEngine;
using System.Collections;

public static class ObjectExtensions  {

	public static T FindObjectOfType<T>(this Object o) where T : Object {
		return (T) Object.FindObjectOfType (typeof(T));
	}
		
}
